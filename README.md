# Bluewave Rouge
The stylesheet used to power the [official Sandia Mesa Poal.co sub](https://poal.co/s/SandiaMesa).

## How to reuse this stylesheet for your Poal.co sub
To reuse this for your Poal.co sub, do the following:
1. Go to https://code.sandiamesa.com/sandiamesa/bluewaverouge/releases and download 'bluewaverouge.(version).zip'.
2. Extract the zip folder.
3. Open bluewaverouge.css in a source code editor such as [Notepad++](https://notepad-plus-plus.org/) and make any modifications to it that you need to for your Poal.co sub.
4. Minify the stylesheet. We recommend using [Andrew Chilton's CSS Minifier](https://cssminifier.com/) to do so.
5. Once you have copied the minified stylesheet to your clipboard, go to your Poal.co sub and click the "Edit Sub Stylesheet" button towards the bottom of the right tab.
6. Paste your minified stylesheet into the "Custom stylesheet" box and click "Save".

## Copyright
Copyright (C) 2020 Sandia Mesa Animation Studios<br>
Bluewave Rouge is licensed under the [MIT License](LICENSE).